sinAPTik
========

What is it?
-----------
This program is basically a hack to solve a somewhat simple problem. I removed
Ubuntu 12.10 and Windows 7 Ultimate from my PC to install Linux Mint Debian Edition.

I've previously used the [uct leg ubuntu mirror](ftp://ftp.leg.uct.ac.za) for my Ubuntu software.
This mirror didn't seem to work with Linux Mint Debian Edition. I love me some challenge!

I could've Google'd better ways to fix this but I figured why not make a java program
that will download whatever thing I'm trying to install---(had time to waste!)

As it stands, this is probably one of the most dumbest and inefficient program I've ever made.

Laungages
----------

Top bar might tell you that I gotz me some Python too here!

- Python
- Java

External Library
-----------------

This uses [ftp4j](http://www.sauronsoftware.it/projects/ftp4j/)



Warning
---------
Do not run outside the U.C.T network. It may use your bandwith as it it will download packages from the [uct leg ubuntu mirror](ftp://ftp.leg.uct.ac.za).

Licence
---------

The licence is for my code --- not including the external library
 
> Copyright (c) 2013, Zola Mahlaza <adeebnqo@gmail.com>
> All rights reserved.
> 
> Redistribution and use in source and binary forms, with or 
> without modification, are permitted provided that the following 
> conditions are met:
> 
> * Redistributions of source code must retain the above copyright notice, 
> this list of conditions and the following disclaimer.
>  
> * Redistributions in binary form must reproduce the above copyright notice, 
> this list of conditions and the following disclaimer in the documentation 
> and/or other materials provided with the distribution.
>  
> * Neither the name of sinAPTik, nor the names of its 
> contributors may be used to endorse or promote products derived from this 
> software without specific prior written permission. 
> 
> THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
> "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
> LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
> A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
> CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
> EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
> PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
> PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
> LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
> NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
> SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
