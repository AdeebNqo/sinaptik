from ftplib import FTP
import ftplib
uct_leg = FTP('ftp.leg.uct.ac.za')
prev = []
def main():
	#global uct_leg
	#uct_leg = FTP('ftp.leg.uct.ac.za')
	uct_leg.login();
	print(uct_leg.getwelcome())
	#going to the debian dir
	uct_leg.cwd('pub/linux/debian/pool/')
	#uct_leg.dir();

	#opening file to write to
	f = open("list.txt","w")
	#listing cur_dir contents
	cur_dir =[]
	cur_dir = uct_leg.nlst();
	list_dir(cur_dir,f)
	f.close()

def list_dir(some_list,write_file):
	for i in some_list:
		if (is_file(i)):
			write_file.write(uct_leg.pwd()+'/'+i+'\n')
		else:
			#print("a folder")
			prev.append(uct_leg.pwd());
			uct_leg.cwd(i);
			list_dir(uct_leg.nlst(),write_file)
			uct_leg.cwd(prev.pop());

def is_file(filename):
	try:
		uct_leg.cwd(filename);
		#moving back
		uct_leg.cwd("..")
		return False;
	except ftplib.error_perm:
		return True;
if __name__=='__main__':
	main()
